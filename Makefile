VAULT_TOKEN ?= ## authentication token
VAULT_ADDR ?= https://vault.engageska-portugal.pt## vault https address
OIDC_CLIENT_ID ?= ""
OIDC_CLIENT_SECRET ?= ""
# OIDC configuration
DEFAULT_ROLE_OIDC ?= ska-telescope-gitlab-users ## default role for openid connect
BOUND_ISSUER ?= "localhost" ## bound issuer for openid connect

.PHONY: all $(MAKECMDGOALS)

.DEFAULT_GOAL := help

.EXPORT_ALL_VARIABLES:

# define overides for above variables in here
-include PrivateRules.mak

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "VAULT_TOKEN: $(VAULT_TOKEN)"
	@echo "VAULT_ADDR: $(VAULT_ADDR)"
	@echo "OIDC_CLIENT_ID: $(OIDC_CLIENT_ID)"
	@echo "OIDC_CLIENT_SECRET: $(OIDC_CLIENT_SECRET)"
	@echo "DEFAULT_ROLE_OIDC: $(DEFAULT_ROLE_OIDC)"
	@echo "BOUND_ISSUER: $(BOUND_ISSUER)"

install_vault: ## install vault command line tool
	@curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
	@sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $$(lsb_release -cs) main"
	@sudo apt-get update
	@sudo apt-get install vault -y

login: ## login into vault with AUTH_TOKEN
	@vault token lookup || vault login $(VAULT_TOKEN)

enable_oidc: login ## enable openid connect
	@if [ $$(vault auth list | grep oidc | wc -l) = 0 ]; then \
		vault auth enable oidc; \
	else \
		echo "oidc already enabled"; \
	fi

configure_oidc: enable_oidc ## configure openid connect with gitlab.com
	@vault write auth/oidc/config oidc_discovery_url="https://gitlab.com" \
		oidc_client_id=$(OIDC_CLIENT_ID) \
		oidc_client_secret=$(OIDC_CLIENT_SECRET) \
		default_role="$(DEFAULT_ROLE_OIDC)" \
		bound_issuer=$(BOUND_ISSUER)

enable_jwt: login ## enable jwt authentication
	@if [ $$(vault auth list | grep jwt | wc -l) = 0 ]; then \
		vault auth enable jwt; \
	else \
		echo "jwt already enabled"; \
	fi

configure_jwt: enable_jwt ## configure jwt authentication
	@vault write auth/jwt/config jwks_url="https://gitlab.com/-/jwks" bound_issuer="gitlab.com"

enable_kv: login ## enable key-value store
	@if [ $$(vault secrets list | grep kv | wc -l) = 0 ]; then \
		vault secrets enable -version=2 kv; \
	else \
		echo "kv already enabled"; \
	fi

write_roles: configure_jwt configure_oidc ## add roles in vault
	@for filename in $$(ls roles/oidc); do \
		vault write auth/oidc/role/$${filename%.*} @roles/oidc/$$filename; \
	done; \
	for filename in $$(ls roles/jwt); do \
		vault write auth/jwt/role/$${filename%.*} @roles/jwt/$$filename; \
	done

write_policies: login ## write policies in vault
	@for filename in $$(ls policies); do \
		vault policy write $${filename%.*} policies/$$filename; \
	done

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
