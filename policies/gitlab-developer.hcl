path "kv/metadata/*" {
  capabilities = ["list", "read"]
}
 
path "kv/data/users/{{identity.entity.aliases.auth_oidc_91a17301.name}}/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}
