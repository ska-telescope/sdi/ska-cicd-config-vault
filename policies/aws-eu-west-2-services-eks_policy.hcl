path "aws-eu-west-2-services-eks/data/*" {
    capabilities = ["read"]
}
path "kv/data/groups/*" {
    capabilities = ["read"]
}
