path "kv/metadata/*" {
  capabilities = ["list", "read"]
}

path "kv/data/groups/ska-dev/atlas" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}
