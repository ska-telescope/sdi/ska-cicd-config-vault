path "ska-low-clp-infrastructure-machinery/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "kv/metadata/*" {
  capabilities = ["list", "read"]
}

path "kv/data/groups/ska-dev/topic" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}
