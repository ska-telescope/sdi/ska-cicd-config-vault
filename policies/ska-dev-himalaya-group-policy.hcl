path "kv/metadata/*" {
  capabilities = ["list", "read"]
}
 
path "kv/data/groups/ska-dev/himalaya" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}
