path "aws-eu-west-2-cicd-eks/data/*" {
    capabilities = ["read"]
}
path "kv/data/groups/*" {
    capabilities = ["read"]
}
