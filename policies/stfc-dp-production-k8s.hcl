path "stfc-dp-production-k8s/data/*" {
    capabilities = ["read"]
}
path "kv/data/groups/*" {
    capabilities = ["read"]
}
path "kv/data/users/*" {
    capabilities = ["read"]
}
