# ska-cicd-config-vault

:bulb: Configure Vault (identity-based secrets and encryption management system) using `make`.

## Summary

This repo provides a basic recipe for configuring [`Vault`](https://www.vaultproject.io/docs/what-is-vault). This is done by installing the vault cli so that it is possible to interact with a vault server. 

## How to Use

Checkout ska-cicd-config-vault (this repo) with:

```
git clone https://gitlab.com/ska-telescope/sdi/ska-cicd-config-vault.git
cd ska-cicd-config-vault
```

Inspect the help and defaults with:

```
$ make
make targets:
Makefile:configure_jwt         configure jwt authentication
Makefile:configure_oidc        configure openid connect with gitlab.com
Makefile:enable_jwt            enable jwt authentication
Makefile:enable_kv             enable key-value store
Makefile:enable_oidc           enable openid connect
Makefile:help                  show this help.
Makefile:install_vault         install vault command line tool
Makefile:login                 login into vault with AUTH_TOKEN
Makefile:vars                  Variables
Makefile:write_policies        write policies in vault
Makefile:write_roles           add roles in vault

make vars (+defaults):
Makefile:BOUND_ISSUER          "localhost" ## bound issuer for openid connect
Makefile:DEFAULT_ROLE_OIDC     ska-telescope-gitlab-users ## default role for openid connect
Makefile:OIDC_CLIENT_ID        ""
Makefile:OIDC_CLIENT_SECRET    ""
Makefile:VAULT_ADDR            https://vault.engageska-portugal.pt## vault https address
Makefile:VAULT_TOKEN           ## authentication token
```

In order for this repo to work a token (VAULT_TOKEN) and vault address (VAULT_ADDR) should be set in a file called `PrivateRules.mak`. Explore the variables with the following command: 

```
$ make vars
Current variable settings:
VAULT_TOKEN: ************************
VAULT_ADDR: https://vault.engageska-portugal.pt
OIDC_CLIENT_ID: 7065cfdccafb10816ec6d5b78dbdf89c63e4f41d08157e9f598df70a923f963c
OIDC_CLIENT_SECRET: ************************************************
DEFAULT_ROLE_OIDC: ska-telescope-gitlab-users
BOUND_ISSUER: localhost
```

A procedure to install the vault cli is given (tested in ubuntu 20) with the target `install_vault`: 

```
$ make install_vault
OK
Hit:1 http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.04  InRelease
Hit:2 https://apt.releases.hashicorp.com focal InRelease
Hit:3 https://download.docker.com/linux/ubuntu focal InRelease
Hit:4 http://ppa.launchpad.net/openjdk-r/ppa/ubuntu xenial InRelease
Hit:5 http://security.ubuntu.com/ubuntu focal-security InRelease
Hit:6 http://archive.ubuntu.com/ubuntu focal InRelease
Hit:7 http://archive.ubuntu.com/ubuntu focal-updates InRelease
Hit:9 http://archive.ubuntu.com/ubuntu focal-backports InRelease
Hit:8 https://packagecloud.io/datawireio/telepresence/ubuntu focal InRelease
Reading package lists... Done
Hit:1 http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.04  InRelease
Hit:2 http://archive.ubuntu.com/ubuntu focal InRelease
Hit:3 https://apt.releases.hashicorp.com focal InRelease
Hit:4 http://archive.ubuntu.com/ubuntu focal-updates InRelease
Hit:5 http://ppa.launchpad.net/openjdk-r/ppa/ubuntu xenial InRelease
Hit:6 https://download.docker.com/linux/ubuntu focal InRelease
Hit:7 http://archive.ubuntu.com/ubuntu focal-backports InRelease
Hit:8 http://security.ubuntu.com/ubuntu focal-security InRelease
Hit:9 https://packagecloud.io/datawireio/telepresence/ubuntu focal InRelease
Reading package lists... Done
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages were automatically installed and are no longer required:
  fonts-gfs-baskerville fonts-gfs-porson libapache-pom-java libcommons-logging-java libcommons-parent-java libfontbox-java libpdfbox-java
  preview-latex-style python3-appdirs python3-distlib python3-filelock tex-gyre
Use 'sudo apt autoremove' to remove them.
The following packages will be upgraded:
  vault
1 upgraded, 0 newly installed, 0 to remove and 527 not upgraded.
Need to get 69.4 MB of archives.
After this operation, 2048 B disk space will be freed.
Get:1 https://apt.releases.hashicorp.com focal/main amd64 vault amd64 1.9.3 [69.4 MB]
Fetched 69.4 MB in 8s (8251 kB/s)
(Reading database ... 168984 files and directories currently installed.)
Preparing to unpack .../archives/vault_1.9.3_amd64.deb ...
Unpacking vault (1.9.3) over (1.9.2) ...
Setting up vault (1.9.3) ...
Generating Vault TLS key and self-signed certificate...
Generating a RSA private key
........++++
.............................................................................................................................................................++++
writing new private key to 'tls.key'
-----
Vault TLS key and self-signed certificate have been generated in '/opt/vault/tls'.
```

Login to the Vault server with the following command (provide the AUTH_TOKEN and AUTH_ADDR in the `PrivateRules.mak` file):

```
$ make login
Key                 Value
---                 -----
accessor            0xPGl7hnneSZLpXDg0HrFbpt
creation_time       1643197339
creation_ttl        0s
display_name        root
entity_id           n/a
expire_time         <nil>
explicit_max_ttl    0s
id                  ************************
meta                <nil>
num_uses            0
orphan              true
path                auth/token/root
policies            [root]
ttl                 0s
type                service

```

It is possible to enable openid connect (OIDC) aithentication throught gitlab with the command `make enable_oidc`. The target will check if oidc is already enabled and only if not it will do it. The command `make configure_oidc` with the parameters `OIDC_CLIENT_ID`, `OIDC_CLIENT_SECRET`, `DEFAULT_ROLE_OIDC` and `BOUND_ISSUER` will configure the authentication method.  Please check [this page](https://docs.gitlab.com/ee/integration/vault.html) for more information. 

It is possible to enable [JWT](https://jwt.io/introduction) aithentication throught gitlab with the command `make enable_jwt`. The target will check if jwt is already enabled and only if not it will do it. The command `make configure_jwt` will configure the authentication method.  Please check [this page](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/) for more information. 

It is possible to enable [KV Secrets Engine](https://www.vaultproject.io/docs/secrets/kv/kv-v2) with the target `make enable_kv`. The target will check if KV Secrets Engine is already enabled and only if not it will do it.

It is possible to add roles and policies to Vault by providing the corresponding files into the folder `policies` and/or `roles`. Policies are expressed in HCL language while roles with json files. The name of the file will correspond to the name of the policy or role. 

Once the file are ready, apply them with the following commands: 

```
$ make write_policies
Key                 Value
---                 -----
accessor            0xPGl7hnneSZLpXDg0HrFbpt
creation_time       1643197339
creation_ttl        0s
display_name        root
entity_id           n/a
expire_time         <nil>
explicit_max_ttl    0s
id                  ************************
meta                <nil>
num_uses            0
orphan              true
path                auth/token/root
policies            [root]
ttl                 0s
type                service
Success! Uploaded policy: ska-telescope-devsecops
```

```
$ make write_roles
Key                 Value
---                 -----
accessor            0xPGl7hnneSZLpXDg0HrFbpt
creation_time       1643197339
creation_ttl        0s
display_name        root
entity_id           n/a
expire_time         <nil>
explicit_max_ttl    0s
id                  ************************
meta                <nil>
num_uses            0
orphan              true
path                auth/token/root
policies            [root]
ttl                 0s
type                service
jwt already enabled
Success! Data written to: auth/jwt/config
oidc already enabled
Success! Data written to: auth/oidc/config
```

